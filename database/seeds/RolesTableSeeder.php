<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $member_role = new \App\Models\RolesModel();
        $member_role->setConnection('mysql_admin');
        $member_role->name = 'Member';
        $member_role->description = 'Simple member';
        $member_role->created_at = Carbon::now();
        $member_role->updated_at = Carbon::now();
        $member_role->save();

        $member_role = new \App\Models\RolesModel();
        $member_role->setConnection('mysql_admin');
        $member_role->name = 'Administrator';
        $member_role->description = 'Application admin';
        $member_role->created_at = Carbon::now();
        $member_role->updated_at = Carbon::now();
        $member_role->save();
    }
}
