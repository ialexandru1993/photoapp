<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Library\AppFaker;

class PopulateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'photo_app:populate {total_users}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate database with dummy values';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $app_faker;

    public function __construct(AppFaker $app_faker)
    {
        parent::__construct();

        $this->app_faker = $app_faker;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // populate users table
        $this->app_faker->populate_database($this->argument('total_users'));
    }
}
