<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

// models
use App\Models\CommentsModel as CommentsModel;

class CommentsController extends Controller
{
    public function add_comment(Request $request) {
        if ($request->isMethod('post')) {
        	$post_id = $request->input('post_id');

        	if(!$post_id) {
        		return redirect()->back();
        	}

            $data = $request->all();

            $validator = Validator::make($data, [
                'comment' => 'required|string|max:255|min:5'
            ]);

            if($validator->fails()) {
            	return redirect()->back()->withErrors($validator)->withInput();
            }

            // save to database
            $comment = new CommentsModel();

            // change database connection to crud
            $comment->setConnection('mysql_crud');
            
            $comment->user_id = \Auth::user()->id;
            $comment->post_id = $post_id;
            $comment->text = $data['comment'];

            $comment->save();

            return redirect()->back();
        }
    }
}
