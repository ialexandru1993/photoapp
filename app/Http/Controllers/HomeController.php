<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// models
use App\Models\PostsModel as PostsModel;

class HomeController extends Controller
{
    const LIMIT_POSTS = 30;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get random photos
        $random_photos = PostsModel::limit(self::LIMIT_POSTS)->inRandomOrder()->get();

        return view('home', ['photos' => $random_photos]);
    }
}
