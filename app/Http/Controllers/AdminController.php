<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LogModel;

class AdminController extends Controller
{
    public function index() {
    	$log_model = new LogModel();
    	$db_log = $log_model->getLog();

    	return view('admin', ['db_log' => $db_log]);
    }
}
