<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// models
use App\User;
use App\Models\PostsModel;
use App\Models\FollowsModel;
use App\Models\UsersProfileCommentsModel;

class ProfileController extends Controller
{
    public function view_profile(User $user, Request $request) {
		if(!$user->profile) {
			return \Redirect::back();	
		}

		// get random photos
		$random_photos = PostsModel::where('user_id', $user->id)->limit(15)->inRandomOrder()->get();

		// last photos
		$last_photos = PostsModel::where('user_id', $user->id)->limit(15)->orderBy('created_at', 'DESC')->get();

		$show_follow_button = (\Auth::user()->id == $user->id) ? false : true;

		$follows_model = new FollowsModel();

		$user_follow_text = $follows_model->get_follow_message(\Auth::user()->id, $user->id);

		return view('user_profile', [
					'user' => $user,
					'random_photos' => $random_photos,
					'last_photos' => $last_photos,
					'show_follow_button' => $show_follow_button,
					'user_follow_text' => $user_follow_text
				]
			);
    }

    public function add_comment(Request $request) {
        if ($request->isMethod('post')) {
        	$user_id = $request->input('user_id');

        	if(!$user_id) {
        		return redirect()->back();
        	}

            $data = $request->all();

            $validator = \Validator::make($data, [
                'comment' => 'required|string|max:255|min:5'
            ]);

            if($validator->fails()) {
            	return redirect()->back()->withErrors($validator)->withInput();
            }

            // save to database
            $comment = new UsersProfileCommentsModel();

            // change database connection to crud
            $comment->setConnection('mysql_crud');

            $comment->user_id = $user_id;
            $comment->comment_user_id = \Auth::user()->id;
            $comment->text = $data['comment'];

            $comment->save();

            return redirect()->back();
        }
    }

    public function edit_profile(Request $request) {
        $user = \Auth::user();

        if ($request->isMethod('post')) {
            $data = $request->all();

            $validator = \Validator::make($data, [
                'name' => 'required|string|max:255|min:5',
                'username' => 'required|string|max:255|min:3',
                'about_me' => 'required|string|max:215|min:10',
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

            if($validator->fails()) {
                return view('edit_profile', ['errors' => $validator->errors(), 'user' => $user]);
            }

            // generate images name
            $avatar_name = time().'_'.md5($request->avatar->getClientOriginalName()).'.'.$request->avatar->getClientOriginalExtension();
            $cover_name = time().'_'.md5($request->cover->getClientOriginalName()).'.'.$request->cover->getClientOriginalExtension();

            $request->avatar->move(public_path('uploads/profiles'), $avatar_name);
            $request->cover->move(public_path('uploads/profiles'), $cover_name);

            // save to database
            $user = \App\User::find($user->id);
            // change database connection to crud
            $user->setConnection('mysql_crud');            
            $user->username = $data['username'];
            $user->name = $data['name'];
            $user->save();

            $user_profile = \App\Models\UsersProfileModel::where('user_id', $user->id)->first();
            // change database connection to crud
            $user_profile->setConnection('mysql_crud');  
            $user_profile->about = $data['about_me'];
            $user_profile->cover_picture_name = '/profiles/'.$cover_name;
            $user_profile->avatar_name = '/profiles/'.$avatar_name;
            $user_profile->save();

            return redirect('view-profile/'.$user->id);
        }

        return view('edit_profile',['user' => $user]);
    }
}
