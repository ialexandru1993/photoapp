<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// models
use App\Models\FollowsModel;

class FollowsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function follow_user(Request $request) {
    	// get follow_user_id
    	$follow_user_id = $request->input('follow_user_id');

    	if(!$follow_user_id) {
			return response()->json([
			    'success' => '0',
			    'errors' => 'Invalid user id!'
			]);
    	}

    	$follow_user = \App\User::where('id', $follow_user_id)->first();

		if(!$follow_user) {
			return response()->json([
			    'success' => '0',
			    'errors' => 'Invalid user id!'
			]);
		}

    	// get current user id
    	$user_id = \Auth::user()->id;

    	if(!$user_id) {
			return response()->json([
			    'success' => '0',
			    'errors' => 'Invalid user id!'
			]);
    	}

    	// follow or unfollow user
    	$follows_model = new FollowsModel();
    	$type = $follows_model->followOrUnfollowUser($user_id, $follow_user_id);
    	$total_follows = $follows_model->getTotalFollowers($follow_user_id);

    	return response()->json([
		    'success' => '1',
		    'total_follows' => $total_follows,
		    'type' => $type
		]);
    }
}
