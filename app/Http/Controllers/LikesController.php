<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// models
use App\Models\LikesModel;

class LikesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function like_photo(Request $request) {
    	// get post id
    	$photo_id = $request->input('post_id');

    	if(!$photo_id) {
			return response()->json([
			    'success' => '0',
			    'errors' => 'Invalid post id!'
			]);
    	}

    	// get current user id
    	$user_id = \Auth::user()->id;

    	if(!$user_id) {
			return response()->json([
			    'success' => '0',
			    'errors' => 'Invalid user id!'
			]);
    	}

    	//like or unlike photo
    	$likes_model = new LikesModel();
    	$type = $likes_model->likeOrUnlikePhoto($photo_id, $user_id);
    	$total_likes = $likes_model->getTotalLikes($photo_id);

		return response()->json([
		    'success' => '1',
		    'total_likes' => $total_likes,
		    'type' => $type
		]);
    }
}
