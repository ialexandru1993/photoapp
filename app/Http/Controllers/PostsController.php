<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

// models
use App\Models\PostsModel as PostsModel;
use App\Models\LikesModel;
use App\Models\FollowsModel;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add_post(Request $request) {
        // upload
        if ($request->isMethod('post')) {
            $data = $request->all();

            $validator = Validator::make($data, [
                'name' => 'required|string|max:255|min:5',
                'description' => 'required|string|max:215|min:10',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

            if($validator->fails()) {
                return view('new_post', ['errors' => $validator->errors()]);
            }

            // generate image name
            $image_name = time().'_'.md5($request->image->getClientOriginalName()).'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads'), $image_name);

            // save to database
            $post = new PostsModel();

            // change database connection to crud
            $post->setConnection('mysql_crud');  
                      
            $post->user_id = \Auth::user()->id;
            $post->name = $request->input('name');
            $post->description = $request->input('description');
            $post->path = $image_name;

            $photo_id = $post->save();

            if($photo_id) {
                return redirect()->route('view_photo', ['id' => $photo_id]);
            }

            return view('new_post');
        }

        // view form
    	return view('new_post');
    }

    public function view_photo(Request $request) {
        if($request->input('id')) {
            $photo_id = $request->input('id');
            $photo = PostsModel::where('id', $photo_id)->get()->first();

            if(!$photo) {
                 return redirect()->route('view_photo');
            }

            $random_photos = PostsModel::where('user_id', $photo->user_id)->where('id', '<>', $photo->id)->limit(15)->inRandomOrder()->get();

            $user_likes_photo = $this->check_if_users_like_photo($photo->id);

            $follows_model = new FollowsModel();

            $user_follow_text = $follows_model->get_follow_message(\Auth::user()->id, $photo->user->id);

            return view('photo', [
                    'current_photo' => $photo,
                    'random_photos' => $random_photos,
                    'user_likes_photo' => $user_likes_photo,
                    'user_follow_text' => $user_follow_text
                ]);
        }

        return view('photo');
    }

    public function check_if_users_like_photo($photo_id) {
        $likes_model = new LikesModel();
        if(\Auth::user()->id && $likes_model->user_likes_photo($photo_id, \Auth::user()->id)) {
            return true;
        }

        return false;
    }
}
