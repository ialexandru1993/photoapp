<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\UsersRoleModel;

class UsersAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if(!$user || $user->role->current_role->id != UsersRoleModel::ADMIN_ROLE) {
            return redirect('/');
        }

        return $next($request);
    }
}
 