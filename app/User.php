<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function followers() {
        return $this->hasMany('App\Models\FollowsModel', 'follower_id', 'id');
    }

    public function profile() {
        return $this->hasOne('App\Models\UsersProfileModel', 'user_id', 'id');
    }

    public function pictures() {
        return $this->hasMany('App\Models\PostsModel', 'user_id', 'id');
    }

    public function role() {
        return $this->hasOne('App\Models\UsersRoleModel', 'user_id', 'id');
    }
}
