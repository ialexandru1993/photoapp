<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersRoleModel extends Model
{
    protected $table = 'users_role';

    protected $connection = 'mysql_read';
    
    protected $fillable = ['user_id', 'role_id'];

    const MEMBER_ROLE = 1;
    const ADMIN_ROLE = 2;

    public function current_role() {
    	return $this->hasOne('App\Models\RolesModel', 'id', 'role_id');
    }
}
