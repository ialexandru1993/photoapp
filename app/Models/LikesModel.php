<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LikesModel extends Model
{
    protected $table = 'likes';

    protected $connection = 'mysql_read';
    
    protected $fillable = ['user_id', 'post_id', 'type'];

    const LIKE_TYPE = 'like';
    const DISLIKE_TYPE = 'dislike';

    public function likeOrUnlikePhoto($post_id, $user_id) {
    	$like = $this->firstOrNew(
    			[
    				'post_id' => $post_id,
    				'user_id' => $user_id
    			]
    		);

        // change database connection to crud
        $like->setConnection('mysql_crud');

    	// like
    	if(!$like->created_at) {
    		$like->save();
    		return self::LIKE_TYPE;
    	}

    	// unlike
    	$like->forceDelete();
    	return self::DISLIKE_TYPE;
    }

    public function getTotalLikes($post_id) {
    	return self::where('post_id', $post_id)
                    ->count();
    }

    public function user_likes_photo($post_id, $user_id) {
    	return self::where('post_id', $post_id)
                    ->where('user_id', $user_id)
                    ->where('type', self::LIKE_TYPE)
                    ->get()
                    ->first();
    }
}
