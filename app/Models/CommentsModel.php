<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentsModel extends Model
{
    protected $table = 'comments';

    protected $connection = 'mysql_read';

    protected $fillable = ['user_id', 'post_id', 'text'];

    public function user() {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
