<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogModel extends Model
{
    protected $table = 'mysql.general_log';

    protected $connection = 'mysql_admin';

    const COMMAND_QUERY_TYPE = 'Query';

	public function getLog() {
		return \DB::connection('mysql_admin')->table('mysql.general_log')->select('user_host', 'thread_id', 'event_time')->where('command_type', self::COMMAND_QUERY_TYPE)->orderBy('event_time', 'DESC')->get()->toArray();
	}    
}
