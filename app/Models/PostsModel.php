<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostsModel extends Model
{
    protected $table = 'posts';

    protected $connection = 'mysql_read';
    
    protected $fillable = ['user_id', 'name', 'description', 'path'];

    public function user() {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function likes() {
    	return $this->hasMany('App\Models\LikesModel', 'post_id', 'id');
    }

    public function comments() {
    	return $this->hasMany('App\Models\CommentsModel', 'post_id', 'id');
    }
}
