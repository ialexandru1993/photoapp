<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersProfileModel extends Model
{
    protected $table = 'users_profile';

    protected $connection = 'mysql_read';
    
    protected $fillable = ['user_id', 'cover_picture_name', 'avatar_name', 'about'];

    public function comments() {
    	return $this->hasMany('App\Models\UsersProfileCommentsModel', 'user_id', 'user_id');
    }
}
