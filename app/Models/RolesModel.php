<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolesModel extends Model
{
    protected $table = 'roles';

    protected $connection = 'mysql_read';
    
    protected $fillable = ['name', 'description'];

}
