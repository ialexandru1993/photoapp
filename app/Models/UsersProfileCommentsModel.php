<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersProfileCommentsModel extends Model
{
    protected $table = 'users_profile_comments';

    protected $connection = 'mysql_read';
    
    protected $fillable = ['user_id', 'comment_user_id', 'text'];

    public function user() {
    	return $this->hasOne('App\User', 'id', 'comment_user_id');
    }
}
