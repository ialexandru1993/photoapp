<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FollowsModel extends Model
{
    protected $table = 'follows';

    protected $connection = 'mysql_read';
    
    protected $fillable = ['user_id', 'follower_id'];

    const FOLLOW_MESSAGES = [
    	'follow' => 'Follow',
    	'unfollow' => 'Unfollow'
    ];

    public function followOrUnfollowUser($user_id, $follow_user_id) {
    	if($user_id == $follow_user_id) {
    		return false;
    	}

    	$follow = $this->firstOrNew(
    			[
    				'follower_id' => $follow_user_id,
    				'user_id' => $user_id
    			]
    		);

        // change database connection to crud
        $follow->setConnection('mysql_crud');

    	// follow
    	if(!$follow->created_at) {
    		$follow->save();
    		return self::FOLLOW_MESSAGES['unfollow'];
    	}

    	// unfollow
    	$follow->forceDelete();
    	return self::FOLLOW_MESSAGES['follow'];
    }

    public function getTotalFollowers($user_id) {
    	return self::where('follower_id', $user_id)
                    ->count();
    }

    public function user_follow_user($user_id, $follows_user_id) {
    	return self::where('user_id', $user_id)
                    ->where('follower_id', $follows_user_id)
                    ->get()
                    ->first();
    }

    public function get_follow_message($user_id, $follows_user_id) {
    	$follow_status = self::where('user_id', $user_id)
                    ->where('follower_id', $follows_user_id)
                    ->get()
                    ->first();
    
        if(!$follow_status) {
        	return self::FOLLOW_MESSAGES['follow'];
        }

        return self::FOLLOW_MESSAGES['unfollow'];
    }
}
