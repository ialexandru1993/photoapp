<?php
// generate fake data

namespace Library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// models
use App\Models\LikesModel;
use App\Models\CommentsModel;
use App\Models\PostsModel;
use App\Models\FollowsModel;
use App\Models\UsersRoleModel;
use App\Models\UsersProfileModel;
use App\Models\UsersProfileCommentsModel;

class AppFaker extends Controller
{
	protected $total_random_pictures = 119;
	protected $random_pictures_path = "/dummy_images/random_picture";
	protected $random_pictures_extension = ".png";

	public function populate_database($total_fakes = 5) {
	    $total_fakes = (int) $total_fakes;

	    $faker = \Faker\Factory::create();

	    $created_users = [];
	    $created_posts = [];
	    $created_likes = [];
	    $created_comments = [];
	    
	    // add users
	    for($i = 0; $i < $total_fakes; $i++) {
	        $user = new \App\User();
	        $user->setConnection('mysql_crud');
	        $user->name = $faker->name;
	        $user->username = $faker->userName;
	        $user->email = $faker->email;
	        $user->password = bcrypt($faker->password);
	        $user->save();

	        // add role to db
	        $user_role = new \App\Models\UsersRoleModel();
	        $user_role->setConnection('mysql_admin');
	        $user_role->user_id = $user->id;
	        $user_role->role_id = \App\Models\UsersRoleModel::MEMBER_ROLE;
	        $user_role->save();

	        $created_users[] = $user;
	    }

	    $follows_model = new FollowsModel();

	    $created_posts = array();

	    // add posts => foreach created user add total_fakes posts
	    foreach($created_users as $user) {
			for($i = 0; $i < $total_fakes; $i++) {
				$file_path = $this->random_pictures_path.sprintf("%'03d", rand(1, $this->total_random_pictures)).$this->random_pictures_extension;

				$posts_model = new PostsModel();
				$posts_model->setConnection('mysql_crud');
				$posts_model->user_id = $user->id;
				$posts_model->name = $faker->name;
				$posts_model->description = $faker->text;
				$posts_model->path = $file_path;
				$posts_model->save();
				
				$created_posts[] = $posts_model;
		    }

		    // follow random user
		    $key = array_rand($created_users);
		    $follow_user = $created_users[$key];
		    $follows_model->followOrUnfollowUser($user->id, $follow_user->id);
			
			// add profile	    	
		    $this->populate_user_profile_table($user);
	    }

	    // foreach created posts add random likes and comments
	    foreach($created_posts as $post) {
	    	foreach($created_users as $user) {
	    		$rand_number = rand(1,6);

	    		// for number 2 add commment, for 3 add like, for 4 add profile comment
	    		if($rand_number == 1) {
	    			continue;
	    		}
	    		if($rand_number == 2) {
	    			$this->populate_likes_table($user->id, $post->id);
	    		}

	    		if($rand_number == 4) {
	    			$this->populate_profile_comments_table($user->id, $post->user_id);
	    		}

	    		$this->popoulate_comments_table($user->id, $post->id);
	    	}
	    }
	}

	public function populate_likes_table($user_id, $post_id) {
		$likes_model = new LikesModel();
		$likes_model->setConnection('mysql_crud');
		$likes_model->user_id = $user_id;
		$likes_model->post_id = $post_id;
		$likes_model->save();
	}

	public function popoulate_comments_table($user_id, $post_id) {
		$faker = \Faker\Factory::create();

		$comments_model = new CommentsModel();
		$comments_model->setConnection('mysql_crud');
		$comments_model->user_id = $user_id;
		$comments_model->post_id = $post_id;
		$comments_model->text = $faker->text;
		$comments_model->save();
	}

	public function populate_user_profile_table($user) {
		$faker = \Faker\Factory::create();

		$users_profile_model = new UsersProfileModel();
		$users_profile_model->setConnection('mysql_crud');
		$users_profile_model->user_id = $user->id;
		$users_profile_model->about = $faker->text;
		$users_profile_model->save();
	}

	public function populate_profile_comments_table($user_id, $comment_user_id) {
		$faker = \Faker\Factory::create();

		$users_profile_comments_model = new UsersProfileCommentsModel();
		$users_profile_comments_model->setConnection('mysql_crud');
		$users_profile_comments_model->user_id = $user_id;
		$users_profile_comments_model->comment_user_id = $comment_user_id;
		$users_profile_comments_model->text = $faker->text;
		$users_profile_comments_model->save();
	}

	public function set_total_random_pictures($total_random_pictures) {
		$this->total_random_pictures = $total_random_pictures;
	}

	public function set_random_pictures_path($random_pictures_path) {
		$this->random_pictures_path = $random_pictures_path;
	}

	public function set_random_picutres_extension($random_pictures_extension) {
		$this->random_pictures_extension = $random_pictures_extension;
	}
}