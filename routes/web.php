<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\UsersAdminRole;


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/add-new-photo', 'PostsController@add_post')->name('new_post');
Route::post('/add-new-photo', 'PostsController@add_post')->name('new_post');

Route::get('/view-photo', 'PostsController@view_photo')->name('view_photo');

Route::post('/like-photo', 'LikesController@like_photo')->name('like_photo');

Route::post('/follow-user', 'FollowsController@follow_user')->name('follow_user');

Route::post('/add-comment', 'CommentsController@add_comment')->name('new_comment');

Route::get('/view-profile/{user}', 'ProfileController@view_profile')->name('view_profile');

Route::post('/add-profile-comment', 'ProfileController@add_comment')->name('profile_new_comment');

Route::get('/edit-profile', 'ProfileController@edit_profile')->name('edit_profile');

Route::post('/edit-profile', 'ProfileController@edit_profile')->name('edit_profile');

Route::get('/admin', 'AdminController@index')->name('view_admin')->middleware(UsersAdminRole::class);