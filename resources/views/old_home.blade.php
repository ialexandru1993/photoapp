@extends('layouts.main')

@section('content')
    <div class="container-fluid inner2 tp0">
      <div class="collage-wrapper">
        <div id="collage-large" class="collage effect-parent light-gallery">
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w1-full.jpg') }}" class="lgitem" data-sub-html="#caption1"><img src="{{ asset('img/art/w1.jpg') }}" style="width:675px; height:450px;" alt="" /></a>
              <div id="caption1" class="hidden">
                <h3>Alex</h3>
                <p>16:33 12.11.2016</p>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w2-full.jpg') }}" class="lgitem" data-sub-html="#caption2"><img src="{{ asset('img/art/w2.jpg') }}" style="width:550px; height:450px;" alt="" /></a>
              <div id="caption2" class="hidden">
                <h3>Nibh Sollicitudin Risus</h3>
                <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w3-full.jpg') }}" class="lgitem"><img src="{{ asset('img/art/w3.jpg') }}" style="width:675px; height:450px;" alt="" /></a> </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w4-full.jpg') }}" class="lgitem" data-sub-html="#caption4"><img src="{{ asset('img/art/w4.jpg') }}" style="width:416px; height:450px;" alt="" /></a>
              <div id="caption4" class="hidden">
                <h3>Nullam Cras Condimentum</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w5-full.jpg') }}" class="lgitem" data-sub-html="#caption5"><img src="{{ asset('img/art/w5.jpg') }}" style="width:675px; height:450px;" alt="" /></a>
              <div id="caption5" class="hidden">
                <h3>Ullamcorper Bibendum Ultricies</h3>
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor.</p>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w6-full.jpg') }}" class="lgitem"><img src="{{ asset('img/art/w6.jpg') }}" style="width:581px; height:450px;" alt="" /></a> </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w7-full.jpg') }}" class="lgitem" data-sub-html="#caption7"><img src="{{ asset('img/art/w7.jpg') }}" style="width:678px; height:450px;" alt="" /></a>
              <div id="caption7" class="hidden">
                <h3>Mollis Sit Bibendum</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w8-full.jpg') }}" class="lgitem" data-sub-html="#caption8"><img src="{{ asset('img/art/w8.jpg') }}" style="width:510px; height:450px;" alt="" /></a>
              <div id="caption8" class="hidden">
                <h3>Malesuada Mollis Cras</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w9-full.jpg') }}" class="lgitem"><img src="{{ asset('img/art/w9.jpg') }}" style="width:678px; height:450px;" alt="" /></a> </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w10-full.jpg') }}" class="lgitem" data-sub-html="#caption10"><img src="{{ asset('img/art/w10.jpg') }}" style="width:300px; height:450px;" alt="" /></a>
              <div id="caption10" class="hidden">
                <h3>Malesuada Tellus Ridiculus</h3>
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w11-full.jpg') }}" class="lgitem" data-sub-html="#caption11"><img src="{{ asset('img/art/w11.jpg') }}" style="width:679px; height:450px;" alt="" /></a>
              <div id="caption11" class="hidden">
                <h3>Cursus Ligula Malesuada</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w12-full.jpg') }}" class="lgitem"><img src="{{ asset('img/art/w12.jpg') }}" style="width:674px; height:450px;" alt="" /></a> </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w13-full.jpg') }}" class="lgitem" data-sub-html="#caption13"><img src="{{ asset('img/art/w13.jpg') }}" style="width:341px; height:450px;" alt="" /></a>
              <div id="caption13" class="hidden">
                <h3>Mollis Vulputate Lorem</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w14-full.jpg') }}" class="lgitem" data-sub-html="#caption14"><img src="{{ asset('img/art/w14.jpg') }}" style="width:577px; height:450px;" alt="" /></a>
              <div id="caption14" class="hidden">
                <h3>Ligula Ullamcorper Fusce</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w15-full.jpg') }}" class="lgitem" data-sub-html="#caption15"><img src="{{ asset('img/art/w15.jpg') }}" style="width:450px; height:450px;" alt="" /></a>
              <div id="caption15" class="hidden">
                <h3>Tellus Consectetur Bibendum</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w16-full.jpg') }}" class="lgitem"><img src="{{ asset('img/art/w16.jpg') }}" style="width:675px; height:450px;" alt="" /></a> </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w17-full.jpg') }}" class="lgitem" data-sub-html="#caption17"><img src="{{ asset('img/art/w17.jpg') }}" style="width:675px; height:450px;" alt="" /></a>
              <div id="caption17" class="hidden">
                <h3>Ipsum Pellentesque Lorem Adipiscing</h3>
                <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris.</p>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w18-full.jpg') }}" class="lgitem" data-sub-html="#caption18"><img src="{{ asset('img/art/w18.jpg') }}" style="width:800px; height:450px;" alt="" /></a>
              <div id="caption18" class="hidden">
                <h3>Condimentum Malesuada Ridiculus</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w19-full.jpg') }}" class="lgitem" data-sub-html="#caption19"><img src="{{ asset('img/art/w19.jpg') }}" style="width:675px; height:450px;" alt="" /></a>
              <div id="caption19" class="hidden">
                <h3>Cras Quam Venenatis</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w20-full.jpg') }}" class="lgitem" data-sub-html="#caption20"><img src="{{ asset('img/art/w20.jpg') }}" style="width:657px; height:450px;" alt="" /></a>
              <div id="caption20" class="hidden">
                <h3>Venenatis Pharetra Commodo</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w21-full.jpg') }}" class="lgitem"><img src="{{ asset('img/art/w21.jpg') }}" style="width:675px; height:450px;" alt="" /></a> </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w22-full.jpg') }}" class="lgitem" data-sub-html="#caption22"><img src="{{ asset('img/art/w22.jpg') }}" style="width:643px; height:450px;" alt="" /></a>
              <div id="caption22" class="hidden">
                <h3>Cursus Venenatis Ullamcorper</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w23-full.jpg') }}" class="lgitem" data-sub-html="#caption23"><img src="{{ asset('img/art/w23.jpg') }}" style="width:336px; height:450px;" alt="" /></a>
              <div id="caption23" class="hidden">
                <h3>Purus Pellentesque Porta</h3>
              </div>
            </div>
          </div>
          <div class="collage-image-wrapper">
            <div class="overlay"><a href="{{ asset('img/art/w24-full.jpg') }}" class="lgitem" data-sub-html="#caption24"><img src="{{ asset('img/art/w24.jpg') }}" style="width:675px; height:450px;" alt="" /></a>
              <div id="caption24" class="hidden">
                <h3>Ligula Pellentesque Parturient</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
