@extends('layouts.main')

@section('content')
<div class="container">
  <p>LOG useri database:</p>            
  <table class="table">
    <thead>
      <tr>
        <th>user</th>
        <th>thread_id</th>
        <th>event_time</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($db_log as $log)
        <tr>
            <td>{{ $log->user_host }}</td>
            <td>{{ $log->thread_id }}</td>
            <td>{{ $log->event_time }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection

@section('scripts')

@endsection