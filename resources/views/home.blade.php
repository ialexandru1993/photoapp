@extends('layouts.main')

@section('content')
    <div class="container-fluid inner2 tp0">
      <div class="collage-wrapper">
        <div id="collage-large" class="collage effect-parent light-gallery">
          @foreach ($photos as $photo)
            <div class="collage-image-wrapper">
              <div class="overlay">
                <a href="{{ URL::to('view-photo?id='.$photo->id) }}" ><img src="{{ asset('uploads/'.$photo->path) }}" />
                </a>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
@endsection
