@extends('layouts.main')

@section('content')
  <div class="row">
      <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
              <div class="panel-heading">Edit your profile</div>

              <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{ route('edit_profile') }}" enctype="multipart/form-data">
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                          <label for="username" class="col-md-4 control-label">Username</label>

                          <div class="col-md-6">
                              <input id="username" type="text" class="form-control" name="username" value="{{ $user->username }}" required autofocus>

                              @if ($errors->has('username'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('username') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <label for="name" class="col-md-4 control-label">Name</label>

                          <div class="col-md-6">
                              <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>

                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('about_me') ? ' has-error' : '' }}">
                          <label for="about_me" class="col-md-4 control-label">About me</label>

                          <div class="col-md-6">
                              <textarea id="about_me" rows="3" name="about_me" required autofocus>{{ $user->profile->about }}</textarea>

                              @if ($errors->has('about_me'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('about_me') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                          <label for="avatar" class="col-md-4 control-label">Avatar</label>

                          <div class="col-md-6">
                               <input type="file" name="avatar">

                              @if ($errors->has('avatar'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('avatar') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('cover') ? ' has-error' : '' }}">
                          <label for="cover" class="col-md-4 control-label">Cover image</label>

                          <div class="col-md-6">
                               <input type="file" name="cover">

                              @if ($errors->has('cover'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('cover') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              <button type="submit" class="btn btn-primary">
                                  Edit
                              </button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
@endsection
