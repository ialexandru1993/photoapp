@extends('layouts.main')

@section('content')
  <div class="container-fluid inner2 tp0">
    @if(isset($current_photo))
      <div class="photo_container">
        <div class="photo_left">
          <div id="collage-large" class="collage  light-gallery">
            <div class="collage-image-wrapper">
              <div class="overlay">
                <a href="{{ asset('uploads/'.$current_photo->path) }}" class="lgitem" data-sub-html="#caption1"><img class="img-responsive" src="{{ asset('uploads/'.$current_photo->path) }}"/></a>
              </div>
            </div>
          </div>
        </div>
        <div class="photo_right">
          <div class="description">
            <span class="info title">
                {{ $current_photo->name}}
                <a id="like_post" class="url_likes">
                  <div class="photo_likes {{ ($user_likes_photo) ? 'like' : '' }}" id="current_photo" style="display: inline;">
                      <span class="total_likes">
                        {{ sizeof($current_photo->likes) }}
                      </span>
                  </div>
                </a>
            </span> 
            <span class="info">{{ $current_photo->description}}</span>
            <span class="info">{{ $current_photo->created_at}}</span>
            <span class="info"><a href="/view-profile/{{ $current_photo->user->id }}">{{ $current_photo->user->username }}</a></span>
            <a id="follow_post" class="url_follow btn btn-primary">
                  {{ $user_follow_text }}
            </a>
            <br/>
            <span class="total_followers">
              <span id="followers_count">{{ sizeof($current_photo->user->followers) }}</span> users follows this profile.
            </span>
          </div>
          <div class="photo_comments">
            <h3>Photo comments</h3>
            @if(sizeof($current_photo->comments))
              @foreach($current_photo->comments as $comment)
                <div class="comment">
                  <div class="top">
                    <span class="user">
                      {{ $comment->user->username }}
                    </span>
                    <span class="date">
                      {{ $comment->created_at }}
                    </span>
                  </div>
                  <div class="content">
                    {!! nl2br(htmlspecialchars($comment->text)) !!}
                  </div>
                </div>
              @endforeach
            @else
              There are no comments! :(
            @endif
              <div class="add_new_comment">
                  <form class="form-horizontal" method="POST" action="{{ route('new_comment') }}" enctype="multipart/form-data">
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                          <div class="col-xs-12">
                          <textarea id="comment" name="comment" value="{{ old('comment') }}" class="form-control" rows="3" placeholder="Comment" required></textarea>

                              @if ($errors->has('comment'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('comment') }}</strong>
                                  </span>
                              @endif
                          </div>

                          <input id="post_id" type="hidden" name="post_id" value="{{ $current_photo->id}}">
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-3 col-xs-12">
                              <button type="submit" class="btn btn-primary">
                                  Add comment
                              </button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
        </div>
      </div>
    @endif
    @if(isset($random_photos))
        <div class="random_photos">
          <h2>Random photos</h2>
          @include('photos_bar', ['photos' => $random_photos])
        </div>
    @endif
  </div>
@endsection

@section('scripts')
  <script>
    @if(isset($current_photo))
      $('#like_post').on('click', function(){
          var post_url = '/like-photo';
          $.ajax(
          {
              url : post_url,
              type: "POST",
              data: {
                "_token": "{{ csrf_token() }}",
                "post_id": {{ $current_photo->id }}
              },
              success:function(data, textStatus, jqXHR)
              {
                if(data.success) {
                  $('.total_likes').html(data.total_likes);
                  if(data.type == 'like') {
                      $('#current_photo').addClass('like');
                      return true;
                  } 
                  $('#current_photo').removeClass('like');
                }
              },
          });
          e.preventDefault();
      });

      $('#follow_post').on('click', function(){
          var post_url = '/follow-user';
          $.ajax(
          {
              url : post_url,
              type: "POST",
              data: {
                "_token": "{{ csrf_token() }}",
                "follow_user_id": {{ $current_photo->user_id }}
              },
              success:function(data, textStatus, jqXHR)
              {
                if(data.success) {
                  $('#followers_count').html(data.total_follows);
                  $('#follow_post').html(data.type);
                }
              },
          });
          e.preventDefault();
      });
    @endif
  </script>
@endsection