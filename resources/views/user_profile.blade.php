@extends('layouts.main')

@section('content')
      <div class="user_profile">  
          <div class="cover_image"> 
              <img src={{ asset('uploads'.$user->profile->cover_picture_name) }}>
          </div>
      </div>

      <div class="container-fluid inner2 tp0">
          <div class="row">
            <div class="col-md-10 col-md-push-1 col-xs-12">
                <div class="user_profile user_profile_content">
                    <div class="avatar">
                      <img src={{ asset('uploads'.$user->profile->avatar_name) }}>
                    </div>
                    <div class="row custom_row">
                      <div class="col-md-3 col-xs-12">
                        <div class="user_name">
                          <span>{{ $user->username }} profile ({{ $user->role->current_role->name }})</span>
                        </div>
                        <div class="join_date">
                          <span>Member since {{ date('d.m.Y', strtotime($user->created_at)) }}</span>
                        </div>
                        <div class="total_followers">
                          <span><b id="followers_count">{{ sizeof($user->followers) }}</b> follows</span>
                        </div>
                        <div class="total_pictures">
                          <span><b>{{ sizeof($user->pictures) }}</b> pictures</span>
                        </div>
                        <div class="left_content">
                          @if($show_follow_button)
                            <div class="follow">
                              <a id="follow_post" class="btn btn-primary follow-btn">{{ $user_follow_text }}</a>
                            </div>
                          @endif
                          @if(sizeof($user->profile->comments ))
                              <div class="comments">
                                  <h4>Profile comments</h4>
                                  @foreach($user->profile->comments as $comment)
                                    <div class="comment">
                                      <div class="top">
                                        <span class="user">
                                          {{ $comment->user->username }}
                                        </span>
                                        <span class="date">
                                          {{ $comment->created_at }}
                                        </span>
                                      </div>
                                      <div class="content">
                                        {!! nl2br(htmlspecialchars($comment->text)) !!}
                                      </div>
                                    </div>
                                  @endforeach
                              </div>
                          @endif
                          <div class="add_new_comment">
                              <form class="form-horizontal" method="POST" action="{{ route('profile_new_comment') }}" enctype="multipart/form-data">
                                  {{ csrf_field() }}

                                  <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                                      <div class="col-xs-12">
                                      <textarea id="comment" name="comment" value="{{ old('comment') }}" class="form-control" rows="3" placeholder="Comment" required></textarea>

                                          @if ($errors->has('comment'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->first('comment') }}</strong>
                                              </span>
                                          @endif
                                      </div>

                                      <input id="user_id" type="hidden" name="user_id" value="{{ $user->id}}">
                                  </div>

                                  <div class="form-group">
                                      <div class="col-md-6 col-md-offset-3 col-xs-12">
                                          <button type="submit" class="btn btn-primary">
                                              Add comment
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-xs-12">
                          <div class="story">
                            <div class="title">About {{ $user->name }}</div>
                            <div class="description">{!! nl2br(htmlspecialchars($user->profile->about)) !!}</div>
                          </div>
                      </div>
                    </div>
                    <div class="fix_margin"></div>
                    @if(isset($last_photos))
                      <div class="row">
                        <div class="random_photos">
                          <h2>Last photos</h2>
                          @include('photos_bar', ['photos' => $last_photos])
                        </div>
                      </div>
                    @endif
                </div>
            </div>
          </div>
      </div>
@endsection

@section('scripts')
  <script>
    @if(isset($user))
      $('#follow_post').on('click', function(){
          var post_url = '/follow-user';
          $.ajax(
          {
              url : post_url,
              type: "POST",
              data: {
                "_token": "{{ csrf_token() }}",
                "follow_user_id": {{ $user->id }}
              },
              success:function(data, textStatus, jqXHR)
              {
                if(data.success) {
                  $('#followers_count').html(data.total_follows);
                  $('#follow_post').html(data.type);
                }
              },
          });
          e.preventDefault();
      });
    @endif
  </script>
@endsection