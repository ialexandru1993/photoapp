@if(isset($photos) && $photos) 
      <div class="collage-wrapper">
        <div id="collage-medium" class="collage effect-parent light-gallery">
        	@foreach ($photos as $photo)
	            <div class="collage-image-wrapper">
                <div class="overlay">
                  <a href="{{ URL::to('view-photo?id='.$photo->id) }}" ><img src="{{ asset('uploads/'.$photo->path) }}" />
                  </a>
                </div>
	            </div>
        	@endforeach
        </div>
      </div>
@endif