@include('layouts.header')
 
<div class="light-wrapper">
	@yield('content')
</div>

@include('layouts.footer')