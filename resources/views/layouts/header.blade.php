<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="style/images/favicon.png">
<title>{{ config('app.name', 'Laravel') }}</title>
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/css/color/blue.css') }}/">
<link rel="stylesheet" type="text/css" href="{{ asset('css/icons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/photo.css') }}">
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700%7CKarla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="pageloader">
<div class="content-wrapper">
  <div class="navbar navbar-default default classic full" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <div class="navbar-brand"><a href="{{ URL::to('/') }}"><img src="{{ asset('img/logo.png') }}" alt="" /></a></div>
        <div class="nav-bars-wrapper">
          <div class="nav-bars-inner">
            <div class="nav-bars" data-toggle="collapse" data-target=".navbar-collapse"><span></span></div>
          </div>
          <!-- /.nav-bars-inner --> 
        </div>
        <!-- /.nav-bars-wrapper --> 
      </div>
      <!-- /.nav-header -->
      
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="#!">Contact <span class="caret"></span></a></li>
          
          @if(Auth::check())
            <li><a href="{{ URL::to('/add-new-photo') }}">Add new photo<span class="caret"></span></a></li>
          @endif

          @guest
              <li><a href="{{ route('login') }}">Login <span class="caret"></span></a></li>
              <li><a href="{{ route('register') }}">Register <span class="caret"></span></a></li>
          @else
              <li><a href="/edit-profile">{{ Auth::user()->name }} <span class="caret"></span></a></li>
              <li><a href="#!">
                  <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                      Logout
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                  <span class="caret"></span></a>
               </li>
          @endguest
        </ul>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!--/.container-fluid --> 
  </div>
  <!--/.navbar -->