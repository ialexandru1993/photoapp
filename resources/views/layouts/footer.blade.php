<footer class="inverse-wrapper">
  <div class="sub-footer">
    <div class="container-fluid inner">
    </div>
  </div>
</footer>
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
@yield('scripts')
</body>
</html>